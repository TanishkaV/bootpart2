var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");//will get the element by element id
var t2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var t3 : HTMLInputElement = <HTMLInputElement>document.getElementById("t3");
function add() // will call the function add
{
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString(); // will change the value to string
}
function sub() // will call the function sub
{
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();  // will change the value to string
}
function div() // will call the function div
{
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();  // will change the value to string
}
function multi() // will call the function multi
{
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString(); // will change the value to string
}