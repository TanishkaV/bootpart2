
function print_table() {
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); //will take the id from html
    var table1:HTMLTableElement = <HTMLTableElement>document.getElementById("table1");// will take the id from html
    var count:number =  1; // new variable assigned default value 1
    var num1:number =  1;  // new variable assigned default value 1
    if(t1.value == ""){
        alert(" Value is to be inserted first ");
        return;
    } // in this if id=t1 is null it will tell to insert the value
    num1 = parseInt(t1.value); // will change number into int
    if(isNaN(num1) ){
        alert(" Numbers only allowed ");
        return;
    } // it will check if num1 is number only
    console.log("initial  "+table1.rows.length);
    while(table1.rows.length > 1) // will check that id table1.rows.length is greater than 1 then it will go inside
    { 
        console.log("deleting "+table1.rows.length);
        table1.deleteRow(1); // for getting into a new row it has to delete the current row
    }
    
    for(count = 1 ; count <= num1 ; count++) { 
            console.log("multiplication ");
            var row :   HTMLTableRowElement = table1.insertRow(); // will insert into the row
            var cell :  HTMLTableCellElement = row.insertCell();  // will insert the record in the cell
            var text :  HTMLInputElement = document.createElement("input"); // will input the text
            text.type = "text";  // it can only read the value 
            text.readOnly= true;
            text.style.textAlign = "center";
            text.value = num1.toString() // conversion to string
            cell.appendChild(text);  //append() method inserts specified content at the end of the selected elements

            var cell :  HTMLTableCellElement = row.insertCell();  // will insert into the  new row
            var text :  HTMLInputElement = document.createElement("input");  // will insert the record in the cell
            text.type = "text";
            text.readOnly= true; // it can only read the value 
            text.style.textAlign = "center";
            text.value = " * " // sign which will be printed
            cell.appendChild(text);  //append() method inserts specified content at the end of the selected elements

            var cell :  HTMLTableCellElement = row.insertCell();    // will insert into the  new row
            var text :  HTMLInputElement = document.createElement("input");  // will insert the record in the cell
            text.type = "text";
            text.readOnly= true;  // it can only read the value 
            text.style.textAlign = "center";
            text.value = count.toString() // // conversion to string
            cell.appendChild(text);  //append() method inserts specified content at the end of the selected elements


            var cell :  HTMLTableCellElement = row.insertCell();  // will insert into the  new row
            var text :  HTMLInputElement = document.createElement("input");  // will insert the record in the cell
            text.type = "text";
            text.readOnly= true;  // it can only read the value 
            text.style.textAlign = "center";
            text.value = " = "  // expression which will give the value after multiplication
            cell.appendChild(text);  //append() method inserts specified content at the end of the selected elements



            var cell :  HTMLTableCellElement = row.insertCell();  // will insert into the  new row
            var text :  HTMLInputElement = document.createElement("input");  // will insert the record in the cell
            text.type = "text";
            text.readOnly= true;  // it can only read the value 
            text.style.textAlign = "center";
            text.value = (count * num1 ).toString() // ans is printed after converting into string
            cell.appendChild(text);  //append() method inserts specified content at the end of the selected elements

        } 
}